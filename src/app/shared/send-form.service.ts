import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Form } from './form.model';

@Injectable()

export class SendFormService {

  constructor(private http: HttpClient) {}

  addForm(form: Form) {
    const body = {
      name: form.name,
      surname: form.surname,
      patronymic: form.patronymic,
      phone: form.phone,
      job: form.job,
      selectType: form.selectType,
      size: form.size,
      comment: form.comment,
    };

    return this.http.post('https://js-group-13-default-rtdb.firebaseio.com/form.json', body).pipe();
  }
}
