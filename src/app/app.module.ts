import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainFormComponent } from './main-form/main-form.component';
import { FormsModule } from '@angular/forms';
import { ValidatePhoneDirective } from './main-form/validate-phone.directive';
import { SendFormService } from './shared/send-form.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    MainFormComponent,
    ValidatePhoneDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [SendFormService],
  bootstrap: [AppComponent]
})
export class AppModule { }
